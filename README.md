This document serves the purpose of providing a guideline for the various frontend products that are being developed by the Coko Foundation.  
The projects should be in sync as far as the following technology choices are concerned.  


## React

* Use stateless components whenever possible.


## CSS

Projects should use a combination of Sass and CSS modules.  
More specifically:
* Sass should be enabled in webpack.
* Invidual component styling should be done with a CSS module with the `.local.scss` extension.


## Typefaces 

As Pubsweet is a set of tools for building publishing workflows, the main element of UI is text.  
Therefore, to get a more comprehensive user experience, without having to draw lines between elements, we decided to use different fonts for different kinds of content.

- Text being written -> Courier Prime Sans (or the Cokourier prime sans we created to respect the SIL licence). 
- Text being edited -> Vollkorn  
- Interface text -> Fira Sans & Fira Sans Condensed. 


### Why those?

- Vollkorn is a typeface that is a free take on the closed-source Minion Pro, which University of California Press use for their books. The idea here it is to give an approximation of the feeling they would get with one of their printed books. It's used for the manuscript in Wax, as well as references to the manuscript in the user interface.

- Courier Sans Prime is the typeface chosen for writing content. It is used to create a clear visual differentiation between the (already written) manuscript and new text (such as a review of the manuscipt for a journal paper). The monospaced, typewriter-looking typeface gives the feeling that the content is not in a published state yet.

- Fira Sans is used for the UI of all Coko tools (including INK). It is a complete font family, with condensed versions and multiple weights / styles and it was designed specifically for the screen. It also has a distinctive look.

### Thoughts about the use of fonts

The users don't have the same feeling when they're writing with a published book-looking font (Vollkorn), as when they're writing with a typewriter one (Courier Prime). The latter gives you the feeling that the content is not yet carved in stone. In Editoria for example, most of the content has already ingested from Word. Therefore, using Vollkorn as the main typeface gives the authors and editors a look at what their content will look like when it's published.  

The Wax editor also uses the Vollkorn font, but it would make sense to have versions of Wax with other typefaces, based on its purpose in the application.  
